import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { Facility, FacilityData } from '../models/interfaces';

const API_ENDPOINT = environment.apiEndpoint;

type Authorization = {
  authorized: boolean;
};

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private httpOptions: Object = {};

  constructor(private httpClient: HttpClient) {
  }

  facilities(): Observable<Facility[]> {
    this.httpOptions = { headers: new HttpHeaders({ 'content-type': 'application/json' }) };
    return this.httpClient.get<Facility[]>(API_ENDPOINT + 'facilities', this.httpOptions)
      .pipe(
        catchError(this.handleError),
        map(result => {
          console.log('result', result);
          return result;
        })
      );
  }

  facilityData(id: number): Observable<FacilityData> {
    this.httpOptions = { headers: new HttpHeaders({ 'content-type': 'application/json' }) };
    return this.httpClient.post<FacilityData>(API_ENDPOINT + 'facilityData', { id }, this.httpOptions)
      .pipe(
        catchError(this.handleError),
        map(result => {
          console.log('result', result);
          return result;
        })
      );
  }

  private handleError(error: HttpErrorResponse) {
    return throwError(error);
  }
}

