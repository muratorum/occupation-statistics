export interface Facility {
  id: number;
  name: string;
}

export interface Capacity {
  id: number;
  totalCapacity: number;
  subscriberCapacity: number;
  transientCapacity: number;
  customCapacity: number;
}

export interface Occupation {
  id: number;
  totalOccupation: number;
  subscriberOccupation: number;
  transientOccupation: number;
  customOccupation: number;
}

export interface FacilityData {
  capacity: Capacity;
  occupation: Occupation;
}