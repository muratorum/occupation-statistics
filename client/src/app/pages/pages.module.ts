import { NgModule } from '@angular/core';
import { NbButtonModule, NbCardModule, NbFormFieldModule, NbIconModule, NbInputModule, NbLayoutModule, NbMenuModule, NbSelectModule, NbSidebarModule } from '@nebular/theme';

import { PagesComponent } from './pages.component';
import { PagesRoutingModule } from './pages-routing.module';
import { RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { UsersComponent } from './users/users.component';
import { DashboardModule } from './dashboard/dashboard.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TotalsComponent } from './dashboard/totals/totals.component';
import { GaugeModule } from 'angular-gauge';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    GaugeModule.forRoot(),
    PagesRoutingModule,
    NbMenuModule.forRoot(),
    NbLayoutModule,
    NbInputModule,
    NbFormFieldModule,
    NbIconModule,
    NbCardModule,
    RouterModule, // RouterModule.forRoot(routes, { useHash: true }), if this is your app.module
    NbSidebarModule.forRoot(), // NbSidebarModule.forRoot(), //if this is your app.module
    NbButtonModule,
    NbSelectModule
  ],
  declarations: [
    PagesComponent,
    HomeComponent,
    UsersComponent,
    DashboardComponent,
    TotalsComponent
  ],
})
export class PagesModule {
}
