import { Component, Input, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiService } from 'src/app/services/api.service';
import { interval } from 'rxjs';
import { AbstractControl, FormBuilder } from '@angular/forms';
import { Facility, FacilityData } from 'src/app/models/interfaces';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  @Input() facilities$: Observable<Facility[]> | undefined;
  facilities: Facility[] = [];
  currentFacilityNum = 3;
  currentFacility = '';
  facilityData$: Observable<FacilityData> | undefined;

  constructor(private apiService: ApiService, public formBuilder: FormBuilder) {
  }

  facilitiesForm = this.formBuilder.group({
    facilityName: ''
  });

  get facility(): AbstractControl | null {
    return this.facilitiesForm.get('facilityName');
  }

  changeFacility(e): void {
    this.currentFacilityNum = e;
    this.findFacility(this.currentFacilityNum);
    this.facilityData$ = this.apiService.facilityData(this.currentFacilityNum);
    if (this.facility) {
      this.facility.setValue(e.target, {
        onlySelf: true
      });
    }
  }

  ngOnInit(): void {
    this.facilities$ = this.apiService.facilities();
    this.facilities$.subscribe(data => {
      this.facilities = data;
      this.findFacility(this.currentFacilityNum);
      this.facilityData$ = this.apiService.facilityData(this.currentFacilityNum);
    });

    interval(12000).subscribe(() => {
      this.apiService.facilityData(this.currentFacilityNum).subscribe(data => {
        console.log('data', data);
      });
    });

  }

  private findFacility(e: number): void {
    this.currentFacility = this.facilities.find(i => {
      return Number(i.id) === Number(e);
    }).name;
  }
}
