import { Component, Input, OnInit } from '@angular/core';
import { FacilityData } from 'src/app/models/interfaces';
import { io } from 'socket.io-client';

@Component({
  selector: 'app-totals',
  templateUrl: './totals.component.html',
  styleUrls: ['./totals.component.scss']
})
export class TotalsComponent implements OnInit {
  private socket: any;
  public data!: FacilityData;

  @Input()
  totals: FacilityData | undefined;

  constructor() {
    this.socket = io('http://127.0.0.1:3000');
    this.socket.on('facilityData', (data: FacilityData) => {
      this.data = data;
    });
  }

  ngOnInit(): void {
  }
}
