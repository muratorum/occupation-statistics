import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { NbMenuItem, NbSidebarService } from '@nebular/theme';

export const MENU_ITEMS: NbMenuItem[] = [
  {
    title: '',
    icon: 'home-outline',
    link: '/pages/dashboard',
    home: true,
  },
  {
    title: '',
    icon: 'people-outline',
    link: '/pages/users',
    home: true,
  }
];

@Component({
  selector: 'app-pages',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './pages.component.html',
  styleUrls: ['./pages.component.scss']
})
export class PagesComponent implements OnInit {

  items = MENU_ITEMS;

  constructor(private readonly sidebarService: NbSidebarService) { }

  ngOnInit(): void {
  }

  toggleSidebar(): boolean {
    this.sidebarService.toggle();
    return false;
  }

}
