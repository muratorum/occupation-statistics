import { Facility, FacilityData } from '../models/interfaces';
import FacilityMock from './facilityData.json';
import FacilitiesMock from './facilities.json';

export class MockData {
  facilities: Facility[] = [];
  facilityData: FacilityData | undefined;
  constructor() {
    this.facilities = FacilitiesMock;
    this.facilityData = FacilityMock;
  }
}