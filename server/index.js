const axios = require("axios");
const qs = require("qs");
const restify = require("restify");
const config = require("./config");
const server = restify.createServer();
const NodeCache = require("node-cache");
const myCache = new NodeCache({ stdTTL: 0 });
// Middleware
const corsMiddleware = require("restify-cors-middleware");
const cors = corsMiddleware({
    origins: ["*"],
});
// Mockdata
let facilities = [{
        id: 6,
        name: "Beemster",
    },
    {
        id: 7,
        name: "deRijp",
    },
    {
        id: 5,
        name: "Fietsenstalling Zaandam",
    },
    {
        id: 3,
        name: "Garage",
    },
    {
        id: 8,
        name: "IPS ACC",
    },
    {
        id: 9,
        name: "IPS DEV",
    },
    {
        id: 4,
        name: "Niek's Garage",
    },
];

let facilityData = {
    id: 3,
    capacity: {
        id: 3,
        totalCapacity: 5000,
        subscriberCapacity: 2500,
        transientCapacity: 2500,
        customCapacity: 80,
    },
    occupation: {
        id: 3,
        totalOccupation: 3891,
        subscriberOccupation: 1504,
        transientOccupation: 1548,
        customOccupation: 0,
    },
};

server.use(restify.plugins.bodyParser());
server.pre(cors.preflight);
server.use(cors.actual);

// Routes
server.get("/facilities", (request, response, next) => {
    response.json(facilities);
});

server.post("/facilityData", (request, response, next) => {
    response.json(facilityData);
});

function authorize() {
    const data = qs.stringify({
        client_id: "cc6dc239-ab52-432a-ab83-75ab7f1d9314",
        client_secret: "a71ec491-91a1-4d6f-9476-79218eb68d8f",
        grant_type: "client_credentials",
        response_type: "id_token",
        scope: "ParkBaseApi",
    });
    const config = {
        method: "post",
        url: "https://ids-ippporsche.ipcontrol.nl:475/connect/token",
        headers: {
            "Content-Type": "application/x-www-form-urlencoded",
        },
        data: data,
    };
    axios(config)
        .then(function(res) {
            const tokenString = res.data.token_type + " " + res.data.access_token;
            myCache.set("TOKEN", tokenString, [3600]);
            console.log("Authorization stored in cache:");
            console.log("tokenString", tokenString);
        })
        .catch(function(error) {
            console.log(error);
        }).body;
}

server.get("/api/facilities", (request, response, next) => {
    const token = myCache.get("TOKEN");
    var config = {
        method: "get",
        url: "https://api-ippporsche.ipcontrol.nl:475/api/v1.0/facilities",
        headers: {
            "Content-Type": "application/x-www-form-urlencoded",
            Authorization: token,
        },
    };
    axios(config)
        .then(function(res) {
            const facilities = res.data;
            console.log("facilities", facilities);
            response.json(facilities);
        })
        .catch(function(error) {
            console.log(error);
        });
});

server.post("/api/facilityData", (request, response) => {
    const id = request.body.id;
    const token = myCache.get("TOKEN");
    const headers = {
        headers: {
            "Content-Type": "application/x-www-form-urlencoded",
            Authorization: token,
        },
    };
    const requestCapacityUrl =
        "https://api-ippporsche.ipcontrol.nl:475/api/v1.0/facilities/" +
        id +
        "/capacity";
    const requestOccupationUrl =
        "https://api-ippporsche.ipcontrol.nl:475/api/v1.0/facilities/" +
        id +
        "/occupation";

    const capacity = axios.get(requestCapacityUrl, headers);
    const occupation = axios.get(requestOccupationUrl, headers);

    axios
        .all([capacity, occupation])
        .then(
            axios.spread((...responses) => {
                const capacityResponse = responses[0];
                const occupationResponse = responses[1];
                const result = {
                    capacity: capacityResponse.data,
                    occupation: occupationResponse.data,
                };
                for (const socket of sockets) {
                    socket.emit("facilityData", result);
                }
                console.log("result", result);
                response.json(result);
            })
        )
        .catch((error) => {
            console.log(error);
        });
});

// Start server
server.listen(config.PORT, () => {
    console.info(`Server is up on port: ${config.PORT}.`);
    authorize();
    console.log("Authorization stored in cache:");
    console.log("tokenString", myCache.get("TOKEN"));
});

// Socket with restify sever
const io = require("socket.io")(server.server);
let sockets = new Set();
let data;

io.on("connection", (socket) => {
    sockets.add(socket);
    socket.emit("facilityData", data);
    socket.on("clientData", (data) => console.log(data));
    socket.on("disconnect", () => sockets.delete(socket));
});