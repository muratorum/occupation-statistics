module.exports = {
    ENV: process.env.NODE_ENV || 'development',
    PORT: process.env.PORT || 3000,
    URL: process.env.BASE_URL || 'http://localhost:3000',
    JWT_SECRET: 'waicyrpq9wathawe346iosthqauweszhgeiu',
    CLIENT_ID: 'cc6dc239-ab52-432a-ab83-75ab7f1d9314',
    CLIENT_SECRET: 'a71ec491-91a1-4d6f-9476-79218eb68d8f',
    PARKBASE_API_URL: 'https://ids-ippporsche.ipcontrol.nl:475/connect'
}